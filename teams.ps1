# enforce tls12 usage
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12


# specify number of network threads to use for ms teams connections
function global:Teams-NetThreads {

    param (

        [Parameter(Mandatory = $False)]
        [int32] $Value = 16

    )

    # set network threads value
    $global:threads = $Value

    # network threads value shouldn't exceed the upper limit of 20
    if ($threads -gt 20) {
        Write-Warning -Message "Value of threads shouldn't exceed the upper limit of 20. Maximum value 20 has been set."
        $global:threads = 20
    }
    else {
    }

}


# connect to microsoft teams, interactive popup based authentication handled within the powershell
function global:Teams-Connect {

    # verify if microsoft teams module is installed
    if (!(Get-InstalledModule).Name.Contains("MicrosoftTeams")) {
        Write-Warning -Message "MicrosoftTeams module installation not found, install it using Teams-AddModules command."
        return
    }
    else {
    }

    try {

        # open connection with ms teams
        Write-Host -Object "`nConnecting to Microsoft Teams..."
        $global:connection = Connect-MicrosoftTeams -ErrorAction SilentlyContinue

    }
    catch {

        # print the error if authentication or connection fails, and break the loop
        Write-Warning -Message $Error[1]
        break

    }
    finally {
        Write-Host -NoNewline -Object "`n"
    }

}


# this function will list ms teams of authenticated user
function global:Teams-GetTeamsList {

    param(

        [Parameter(Mandatory = $False)]
        [switch] $OutputFile

    )

    # verify if microsoft teams module is installed
    if (!(Get-InstalledModule).Name.Contains("MicrosoftTeams")) {
        Write-Warning -Message "MicrosoftTeams module installation not found, install it using Teams-AddModules command."
        return
    }
    else {
    }

    # call Teams-Connect function if connection is absent
    if ($null -eq $connection.Account.Id) {
        Teams-Connect
    }
    else {
    }

    # get list of teams
    Write-Host -Object "Getting list of teams for $($connection.Account.Id) using $threads network threads... "
    $list = Get-Team -NumberOfThreads $threads -User $connection.Account.Id

    # save list of teams as an outputfile on desktop
    if ($OutputFile) {
        $filename = "$home\Desktop\$($MyInvocation.MyCommand.Name)-$(Get-Date -Format "MMddyyyy-HHmmss-ffff").Log"
        Write-Host -Object "Teams list saved at location $filename."
        $list.DisplayName | Out-File -FilePath $filename -Force
    }

    # display list of teams on console
    else {
        Write-Host -NoNewline -Object "`n"
        [console]::ForegroundColor = "DarkGreen"; $list.DisplayName; [console]::ForegroundColor = "White"
    }

    Write-Host -NoNewline -Object "`n"

}

# this function will list users from specified ms team
function global:Teams-GetUsersList {

    param(

        [Parameter(Mandatory = $True)]
        [string] $TeamName,

        [Parameter(Mandatory = $False)]
        [switch] $OutputFile

    )

    # verify if microsoft teams module is installed
    if (!(Get-InstalledModule).Name.Contains("MicrosoftTeams")) {
        Write-Warning -Message "MicrosoftTeams module installation not found, install it using Teams-AddModules command."
        return
    }
    else {
    }

    # call Teams-Connect function if connection is absent
    if ($null -eq $connection.Account.Id) {
        Teams-Connect
    }
    else {
    }

    # obtain group id for the target team
    Write-Host -Object "`nGetting details for team $TeamName using $threads network threads...`n"
    $groupid = (Get-Team -NumberOfThreads $threads -DisplayName $TeamName).GroupId

    # make sure target team specified is valid
    if ($null -eq $groupid) {
        Write-Warning -Message "$TeamName team doesn't exist.`n"
        return
    }
    else {
        $list = Get-TeamUser -GroupId $groupid
        if ($OutputFile) {
            $filename = "$home\Desktop\$($MyInvocation.MyCommand.Name)-$(Get-Date -Format "MMddyyyy-HHmmss-ffff").Log"
            Write-Host -Object "Users list saved at location $filename.`n"
            $list | Select-Object -Property User, Name, Role | Out-File -FilePath $filename -Force
        }
        else {
            $list = $list | Select-Object -Property User, Name, Role | Format-Table -AutoSize -Force
            Write-Host -NoNewline -Object "`n"
            [console]::ForegroundColor = "DarkGreen"; $list; [console]::ForegroundColor = "White"
        }
    }

}


# this function will add multiple users to a single team
function global:Teams-MultiUserToTeam {

    param(

        [Parameter(Mandatory = $True)]
        [string] $UsersFile,

        [Parameter(Mandatory = $True)]
        [string] $TeamName,

        [Parameter(Mandatory = $False)]
        [switch] $OutputFile

    )

    # verify if microsoft teams module is installed
    if (!(Get-InstalledModule).Name.Contains("MicrosoftTeams")) {
        Write-Warning -Message "MicrosoftTeams module installation not found, install it using Teams-AddModules command."
        return
    }
    else {
    }

    # verify if the specified path for the usersfile is valid
    if (Test-Path -Path $UsersFile) {

        # verify if teams connection exist else create one
        if ($connection.Account.Id -eq $null) {
            Teams-Connect
        }
        else {
        }

        # obtain group id for the target team
        Write-Host -Object "Getting details for team $TeamName using $threads network threads...`n"
        $groupid = (Get-Team -NumberOfThreads $threads -DisplayName $TeamName).GroupId

        # make sure target team specified is valid
        if ($groupid -eq $null) {
            Write-Warning -Message "$TeamName team doesn't exist.`n"
            return
        }
        else {

            # remove unwanted spaces or newlines from the specified usersfile
            $users = (Get-Content -Path $UsersFile | Where-Object {$_ -ne ""}).Trim()

            # create an output file on desktop
            if ($OutputFile) {
                $filename = "$home\Desktop\$($MyInvocation.MyCommand.Name)-$(Get-Date -Format "MMddyyyy-HHmmss-ffff").Log"
                New-Item -Path $(Split-Path -Path $filename -Parent) -ItemType File -Force -Name $(Split-Path -Path $filename -Leaf) | Out-Null
            }
            else {
            }

            # start adding users to the target team
            Write-Host -Object "Adding multiple users to the team $TeamName...`n"
            $users | ForEach-Object {
                $user = "$_"
                try {
                    Add-TeamUser -GroupId $groupid -User $user -ErrorAction SilentlyContinue
                    if ($OutputFile) {
                        "$user added to the team $TeamName." | Out-File -FilePath $filename -Append -Force
                    }
                    else {
                        [console]::ForegroundColor = "DarkGreen"
                        "$user added to the team $TeamName."
                        [console]::ForegroundColor = "White"
                    }
                }
                catch {
                    if ($OutputFile) {
                        "$user couldn't be added." | Out-File -FilePath $filename -Append -Force
                    }
                    else {
                        [console]::ForegroundColor = "Yellow"
                        "$user couldn't be added."
                        [console]::ForegroundColor = "White"
                    }
                }
                finally {
                }
            }

            if ($OutputFile) {
                Write-Host -Object "Output saved at location $filename."
            }
            else {
            }
            Write-Host -NoNewline -Object "`n"

        }

    }
    else {
        Write-Warning -Message "$UsersFile doesn't exist."
    }

}


# this function will add a single user to multiple teams
function global:Teams-UserToMultiTeam {

    param(

        [Parameter(Mandatory = $True)]
        [string] $TeamsFile,

        [Parameter(Mandatory = $True)]
        [string] $UserName,

        [Parameter(Mandatory = $False)]
        [switch] $OutputFile

    )

    # verify if microsoft teams module is installed
    if (!(Get-InstalledModule).Name.Contains("MicrosoftTeams")) {
        Write-Warning -Message "MicrosoftTeams module installation not found, install it using Teams-AddModules command."
        return
    }
    else {
    }

    # verify if the specified path for the teamsfile is valid
    if (Test-Path -Path $TeamsFile) {

        # verify if teams connection exist else create one
        if ($connection.Account.Id -eq $null) {
            Teams-Connect
        }
        else {
        }

        # make sure target user specified is valid
        Write-Host -Object "Getting details for user $UserName using $threads network threads...`n"
        try {
            $userteams = Get-Team -NumberOfThreads $threads -User $UserName -ErrorAction SilentlyContinue
        }
        catch {
            Write-Warning -Message "$UserName user doesn't exist."
            Write-Host -NoNewline -Object "`n"
            return
        }
        finally {
        }

        # remove unwanted spaces or newlines from the specified teamsfile
        $teams = (Get-Content -Path $TeamsFile | Where-Object {$_ -ne ""}).Trim()

        # create an output file on desktop
        if ($OutputFile) {
            $filename = "$home\Desktop\$($MyInvocation.MyCommand.Name)-$(Get-Date -Format "MMddyyyy-HHmmss-ffff").Log"
            New-Item -Path $(Split-Path -Path $filename -Parent) -ItemType File -Force -Name $(Split-Path -Path $filename -Leaf) | Out-Null
        }
        else {
        }

        # start adding teams membership to the target user
        Write-Host -Object "Adding $UserName to multiple teams...`n"
        $teams | ForEach-Object {
            $team = "$_"

            # obtain group id of the team
            $groupid = (Get-Team -NumberOfThreads $threads -DisplayName $team).GroupId

            # make sure the team name is valid
            if ($groupid -eq $null) {
                if ($OutputFile) {
                    "$UserName couldn't be added to team $team." | Out-File -FilePath $filename -Append -Force
                }
                else {
                    [console]::ForegroundColor = "Yellow"
                    "$UserName couldn't be added to team $team."
                    [console]::ForegroundColor = "White"
                }
            }
            else {
                Add-TeamUser -GroupId $groupid -User $UserName -ErrorAction SilentlyContinue
                if ($OutputFile) {
                    "$UserName added to the team $team." | Out-File -FilePath $filename -Append -Force
                }
                else {
                    [console]::ForegroundColor = "DarkGreen"
                    "$UserName added to the team $team."
                    [console]::ForegroundColor = "White"
                }
            }
        }

        if ($OutputFile) {
            Write-Host -Object "Output saved at location $filename."
        }
        else {
        }
        Write-Host -NoNewline -Object "`n"

    }
    else {
        Write-Warning -Message "$TeamsFile doesn't exist."
    }

}


function global:Teams-AddModules {

    # set psgallery as trusted repository
    if ((Get-PSRepository -Name PSGallery).InstallationPolicy -ne "Trusted") {
        Write-Host -Object "`nSetting PSGallery as trusted repository..."
        Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
    }
    else {
        if ((Get-PSRepository -Name PSGallery).InstallationPolicy -eq "Trusted") {
            Write-Host -Object "`nPSGallery already set as trusted repository..."
        }
    }

    # install/upgrade package provider dependencies
    Write-Host -NoNewline -Object "`n"
    @("NuGet") | ForEach-Object {
        Write-Host -Object "Installing $_ package provider..."
        Install-PackageProvider -Name $_ -Scope AllUsers -Force | Out-Null
    }

    # unload existing modules to prevent upgrade failure
    Teams-UnloadModules

    # load old versions of existent modules before the install/upgrade
    @("PackageManagement", "PowerShellGet") | ForEach-Object {
        Write-Host -Object "Loading built-in module $_ 1.0.0.1..."
        Import-Module -Name $_ -MaximumVersion 1.0.0.1
    }

    # install/upgrade module dependencies
    Write-Host -Object "Checking updates for PowerShell PackageManagement modules..."
    @("NuGet", "PackageManagement", "PowerShellGet") | ForEach-Object {
        $module = $_
        $latestversion = (Find-Module -Name $module).Version
        $installedversion = (Get-InstalledModule -Name $module -ErrorAction SilentlyContinue).Version
        if ($installedversion -lt $latestversion) {
            Write-Host -Object "Installing/Upgrading module $module..."
            Install-Module -Name $module -Scope AllUsers -Force
        }
        else {
            Write-Host -Object "Module $module already of latest version."
        }
    }

    # install/upgrade microsoft teams module
    Write-Host -NoNewline -Object "`n"
    @("MicrosoftTeams") | ForEach-Object {
        $module = $_
        $latestversion = (Find-Module -Name $module).Version
        $installedversion = (Get-InstalledModule -Name $module -ErrorAction SilentlyContinue).Version
        if ($installedversion -lt $latestversion) {
            Write-Host -Object "Installing/Upgrading module $module..."
            Install-Module -Name $module -Scope AllUsers -Force
        }
        else {
            Write-Host -Object "Module $module already of latest version."
        }
    }

    # call the function Teams-LoadModules to load the ms teams module
    Teams-LoadModules

}


# loads the ms teams module
function global:Teams-LoadModules {

    # verify if microsoft teams module is installed
    if (!(Get-InstalledModule).Name.Contains("MicrosoftTeams")) {
        Write-Warning -Message "MicrosoftTeams module installation not found, install it using Teams-AddModules command."
        return
    }
    else {
    }

    # load ms teams module
    Write-Host -Object "`nLoading module Microsoft Teams...`n"
    Import-Module -Name "MicrosoftTeams"

}


# unloads any modules loaded by this script
function global:Teams-UnloadModules {

    Write-Host -NoNewline -Object "`n"
    @("NuGet", "PackageManagement", "PowerShellGet", "MicrosoftTeams") | ForEach-Object {

        # unload module only if it is loaded
        if (Get-Module -Name $_) {
            Write-Host -Object "Unloading module $_..."
            Remove-Module -Name $_ -Force
        }
        else {
            Write-Host -Object "Module $_ already not loaded..."
        }

    }
    Write-Host -NoNewline -Object "`n"

}


# this function will display usage help on console
function global:Teams-Help {

    Write-Host -NoNewline -Object "`n"

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Teams-AddModules`t`t"
    Write-Host -NoNewline -Object "Installs/Upgrades MS Teams Powershell module and other dependent modules."
    Write-Host -ForegroundColor Yellow -Object " (RunAsAdmin)"

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Teams-Connect`t`t`t"
    Write-Host -Object "Establishes MS Teams connection; existing connection will be reset."

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Teams-LoadModules`t`t"
    Write-Host -Object "Loads MS Teams PowerShell module."

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Teams-UnloadModules`t`t"
    Write-Host -Object "Unloads MS Teams PowerShell module and other dependent modules."

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Teams-NetThreads"
    Write-Host -NoNewline -Object " 16`t`t"
    Write-Host -Object "Specify number of network threads to use. 16 is the default value and 20 is the maximum value."

    Write-Host -NoNewline -Object "`n"
    Write-Host -NoNewline -ForegroundColor Yellow -Object "Teams-GetTeamsList`t`t"
    Write-Host -Object "Get list of MS Teams."
    Write-Host -ForegroundColor Yellow -NoNewline -Object "  -OutputFile`t`t`t"
    Write-Host -Object "Create an output file on desktop as well with timestamp."

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Teams-GetUsersList`t`t"
    Write-Host -Object "Get list of users in a specified MS Team."
    Write-Host -ForegroundColor Yellow -NoNewline -Object "  -OutputFile`t`t`t"
    Write-Host -Object "Create an output file on desktop as well with timestamp."

    Write-Host -NoNewline -Object "`n"
    Write-Host -NoNewline -ForegroundColor Yellow -Object "Teams-MultiUserToTeam`t`t"
    Write-Host -Object "Add multiple users to a single team."
    Write-Host -ForegroundColor Yellow -NoNewline -Object "  -UsersFile"
    Write-Host -NoNewline -Object " `"c:\users.txt`"`t"
    Write-Host -Object "Specify plain-text file containing list of users."
    Write-Host -ForegroundColor Yellow -NoNewline -Object "  -TeamName"
    Write-Host -NoNewline -Object " `"Test Team`"`t`t"
    Write-Host -Object "Specify name of the target team."
    Write-Host -ForegroundColor Yellow -NoNewline -Object "  -OutputFile`t`t`t"
    Write-Host -Object "Create an output log file on desktop as well with timestamp."

    Write-Host -NoNewline -ForegroundColor Yellow -Object "Teams-UserToMultiTeam`t`t"
    Write-Host -Object "Add single user to multiple teams."
    Write-Host -ForegroundColor Yellow -NoNewline -Object "  -TeamsFile"
    Write-Host -NoNewline -Object " `"c:\teams.txt`"`t"
    Write-Host -Object "Specify plain-text file containing list of Teams."
    Write-Host -ForegroundColor Yellow -NoNewline -Object "  -UserName"
    Write-Host -NoNewline -Object " `"test@test.com`"`t"
    Write-Host -Object "Specify email (UPN) of the user."
    Write-Host -ForegroundColor Yellow -NoNewline -Object "  -OutputFile`t`t`t"
    Write-Host -Object "Create an output log file on desktop as well with timestamp."

    Write-Host -NoNewline -Object "`n"
    Write-Host -NoNewline -ForegroundColor Yellow -Object "Teams-Help`t`t`t"
    Write-Host -Object "Shows this help page."

    Write-Host -NoNewline -Object "`n"

}


<# .psm1 code

    # list of functions to export
    Export-ModuleMember -Function Teams-AddModules, Teams-NetThreads, Teams-GetTeamsList, Teams-MultiUserToTeam, Teams-UserToMultiTeam, Teams-Help

#>

# set ms teams network threads value to first run default
Teams-NetThreads

# show help on first run
Teams-Help

# call the function to load the ms teams module
Teams-LoadModules
