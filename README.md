# Microsoft Teams

## teams.ps1

* This script will help you to manage teams using automation functions.
* Clone this repository on desktop and import the script as module.

`Import-Module -FullyQualifiedName "$home\Desktop\microsoft-teams\teams.ps1" -DisableNameChecking -Force`

* You can then use below commands to manage Microsoft Teams.

```
Teams-AddModules                Installs/Upgrades MS Teams Powershell module and other dependent modules. (RunAsAdmin)
Teams-Connect                   Establishes MS Teams connection; existing connection will be reset.
Teams-LoadModules               Loads MS Teams PowerShell module.
Teams-UnloadModules             Unloads MS Teams PowerShell module and other dependent modules.
Teams-NetThreads 16             Specify number of network threads to use. 16 is the default value and 20 is the maximum value.

Teams-GetTeamsList              Get list of MS Teams.
  -OutputFile                   Create an output file on desktop as well with timestamp.
Teams-GetUsersList              Get list of users in a specified MS Team.
  -OutputFile                   Create an output file on desktop as well with timestamp.

Teams-MultiUserToTeam           Add multiple users to a single team.
  -UsersFile "c:\users.txt"     Specify plain-text file containing list of users.
  -TeamName "Test Team"         Specify name of the target team.
  -OutputFile                   Create an output log file on desktop as well with timestamp.
Teams-UserToMultiTeam           Add single user to multiple teams.
  -TeamsFile "c:\teams.txt"     Specify plain-text file containing list of Teams.
  -UserName "test@test.com"     Specify email (UPN) of the user.
  -OutputFile                   Create an output log file on desktop as well with timestamp.

Teams-Help                      Shows this help page.
```
